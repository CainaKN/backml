const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const util = require('util');
const request = require('request');
const https = require('https');

const config = {
  clientId: 'SEU_ID',
  clientSecret: 'SEU_SECRET'
}

const app = express()
app.use(cors({}))
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

https.globalAgent.options.secureProtocol = 'SSLv3_method'
app.use(bodyParser.json())

//Autenticação
app.get('/auth/mercadolivre', function (req, res) {
  const authCallBack = BaseUrl() + '/auth/mercadolivre/callback'; //Altere sua base url
  const redirectUrl = util.format('https://auth.mercadolivre.com.br/authorization?response_type=code&client_id=%s&redirect_uri=%s',
  config.clientId, authCallBack);
  res.redirect(redirectUrl);
})

//Autorização
app.get('/auth/mercadolivre/callback', function (req,res) {
  const code = req.query.code;
  console.log('code', code);
  const authCallBack = BaseUrl() + '/auth/mercadolivre/callback'; //Altere sua base url
  const accessTokenUrl = util.format('https://api.mercadolibre.com/oauth/token?grant_type=authorization_code&client_id=%s&client_secret=%s&code=%s&redirect_uri=%s',
  config.clientId, config.clientSecret, code, authCallBack);
  request.post(accessTokenUrl, function(error, response, body) {
    console.log('body', body);
    res.send(body);
  });
});
 
app.listen(3000)